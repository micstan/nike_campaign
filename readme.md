### Causal inference from observational data - impact study of Colin Kaepernick Nike campaign.

- [notebook preview](https://micstan.gitlab.io/share_zone/nike_notebook.nb.html)
- In the second half of 2018 Nike invited Colin Kaepernick as a face of its new global advertising campaign. Kaepernick is a football player but also civil right activist popular of protesting police voilance and killings of African American.
- The campaign rolled out at the begining of September 2018 and positioned Nike in the middle of the ongoing political debate. It resulted with strong polarization of reactions varying from enthusiastic support to organized boykots including public burnnig of company's products.
- In this short analysis I try to quantify the impact of the campaign by looking at publickly available data from Google searches trends as well as Nike stock.
- Stock exchange closing prices are downloaded using [`tidyquant`](https://cran.r-project.org/web/packages/tidyquant/index.html) and for search results I use [`gtrendsR`](https://cran.r-project.org/web/packages/gtrendsR/index.html) wrapper around Google search trends API.
- It is a hard task (but common request) to estimate intervention effect on time series data without randomized experiment setup. In this example the inference is done using amazing [`CausalImpact`](https://github.com/google/CausalImpact) package ([paper](https://ai.google/research/pubs/pub41854)).
